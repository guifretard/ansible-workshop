# Lab 4 - Communication avec externe
## Objectif 

Créer un playbook contenant plusieurs tâches qui enverront un message dans un canal Slack mais en utilisant différentes techniques. La première tâche utilisera le module URI pour envoyer un message standard. Les deux secondes tâches enverront 2 messages avec des couleurs différentes. 

Objectifs : 
- Envoyer le message en utilisant le module API (URI) 
“Message de POD X (votre pod) avec l’API”
- Envoyer le message en utilisant le module Slack
“Ceci est un succès de POD X” de couleur vert (good)
- Envoyer le message en utilisant le module Slack
“Ceci est un avertissement de POD X” de couleur jaune (warning)

URL Slack: https://hooks.slack.com/services/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

Jeton Slack: TPKF8NKNV/BPEER7L11/QGSs1TdOvVchGYCWua2uv6fX
